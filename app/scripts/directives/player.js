'use strict';

/**
 * @ngdoc directive
 * @name bandkitApp.directive:player
 * @description
 * # player
 */
angular.module('bandkitApp')
  .directive('player', function (dataService) {
    return {
      templateUrl: 'views/player.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {

        var playUrl='images/theme/player/play.svg';
        var nextUrl='images/theme/player/next-track.svg';
        var previousUrl='images/theme/player/previous-track.svg';
        var stopUrl='images/theme/player/stop.svg';

        scope.playButton=playUrl;
        scope.nextButton=nextUrl;
        scope.previousButton=previousUrl;




        var playlist={
          "audio":[
            {
              "url":"",
              "title":"loading..."
            }
          ]
        };



        var repeat=false;

        scope.trackNum=0;
        scope.audio = new Audio();
        scope.audio.title = playlist.audio[scope.trackNum].title;

        dataService.async().then(function(d) {
          scope.data = d;
          scope.audio = new Audio(scope.data.songs[scope.trackNum].url);
          scope.audio.title = scope.data.songs[scope.trackNum].title;


          scope.audio.onended=function(){
            console.log("track: "+scope.trackNum+"lengh: "+scope.data.songs.length);
            if(scope.trackNum<scope.data.songs.length-1){
              scope.trackNum+=1;
              //console.log(scope.data.songs[scope.trackNum]);
              scope.audio.src=scope.data.songs[scope.trackNum].url;
              scope.audio.load();
              scope.audio.play();
            }
            else{
              scope.playButton=playUrl;
              scope.trackNum=0;
              scope.audio.src=scope.data.songs[scope.trackNum].url;
              scope.audio.title = scope.data.songs[scope.trackNum].title;
              scope.audio.load();
              scope.audio.play();
            }

          };

          scope.play=function(){
            if(scope.audio.paused===true){
              scope.audio.play();
              scope.playButton=stopUrl;
              //console.log(scope.audio.title);
            }
            else{
              scope.audio.pause();
              scope.playButton=playUrl;
            }
          };

          scope.previous=function(){
            var paused=scope.audio.paused;
            if(scope.trackNum>0){
              scope.trackNum-=1;
              scope.audio.src=scope.data.songs[scope.trackNum].url;
              scope.audio.title = scope.data.songs[scope.trackNum].title;
              scope.audio.load();
              if(!paused){
                scope.audio.play();
              }
            }
          };

          scope.next=function(){
            var paused=scope.audio.paused;
            if(scope.trackNum<scope.data.songs.length-1){
              scope.trackNum+=1;
              scope.audio.src=scope.data.songs[scope.trackNum].url;
              scope.audio.title = scope.data.songs[scope.trackNum].title;
              scope.audio.load();
              if(!paused){
                scope.audio.play();
              }

            }
            else{
              scope.audio.pause();
              scope.playButton=playUrl;
              //scope.$apply();
            }
          };
        });




      }
    };
  });
