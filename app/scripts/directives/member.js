'use strict';

/**
 * @ngdoc directive
 * @name bandkitApp.directive:member
 * @description
 * # member
 */
angular.module('bandkitApp')
  .directive('member', function () {
  return {
    templateUrl: 'views/member.html',
    restrict: 'E',
    link: function postLink(scope, element, attrs) {



    }
  };
});
