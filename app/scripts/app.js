'use strict';

/**
 * @ngdoc overview
 * @name bandkitApp
 * @description
 * # bandkitApp
 *
 * Main module of the application.
 */
angular
  .module('bandkitApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/tour', {
        templateUrl: 'views/tour.html',
        controller: 'TourCtrl',
        controllerAs: 'tour'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl',
        controllerAs: 'contact'
      })
      .when('/impressum', {
        templateUrl: 'views/impressum.html',
        controller: 'ImpressumCtrl',
        controllerAs: 'impressum'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
