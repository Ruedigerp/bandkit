'use strict';

/**
 * @ngdoc function
 * @name bandkitApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the bandkitApp
 */
angular.module('bandkitApp')
  .controller('HeaderCtrl', function ($scope,$location) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.isActive = function (viewLocation) {
      return viewLocation === $location.path();
    };
  });
