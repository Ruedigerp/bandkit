'use strict';

/**
 * @ngdoc function
 * @name bandkitApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the bandkitApp
 */
angular.module('bandkitApp')
  .controller('MainCtrl', function ($scope, dataService) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

  });
