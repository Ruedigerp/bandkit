'use strict';

/**
 * @ngdoc function
 * @name bandkitApp.controller:ImpressumCtrl
 * @description
 * # ImpressumCtrl
 * Controller of the bandkitApp
 */
angular.module('bandkitApp')
  .controller('ImpressumCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
