'use strict';

/**
 * @ngdoc function
 * @name bandkitApp.controller:TourCtrl
 * @description
 * # TourCtrl
 * Controller of the bandkitApp
 */
angular.module('bandkitApp')
  .controller('TourCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
