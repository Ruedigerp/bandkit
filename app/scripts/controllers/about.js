'use strict';

/**
 * @ngdoc function
 * @name bandkitApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bandkitApp
 */
angular.module('bandkitApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
