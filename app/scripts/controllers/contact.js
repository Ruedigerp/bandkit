'use strict';

/**
 * @ngdoc function
 * @name bandkitApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the bandkitApp
 */
angular.module('bandkitApp')
  .controller('ContactCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
